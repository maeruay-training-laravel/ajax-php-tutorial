$.ajax({
    type: 'GET',
    url: `${window.origin}/ajax-php-tutorial/backend/api/v1/user/info.php`,
    headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("accessToken")}`
    },
    data: JSON.stringify({}),
}).done(function (res) {
    let val = JSON.parse(res);
    document.getElementById('user-item').innerHTML = '' +
        '<br>\n' +
        '<li class="nav-item active">\n' +
        '<a class="nav-link" data-toggle="modal" data-target="#modal-profile">'+
        val.message.name
        +'<span class="sr-only">(current)</span></a>\n' +
        '                        </li>\n';
});

function login() {
    console.log(document.getElementById('modal-login'));
    console.log(document.getElementById('login-email').value);
    console.log(document.getElementById('login-password').value);
    document.getElementById('login-submit').style.display = "none";
    document.getElementById('login-spinner').style.display = "block";

    $.ajax({
        type: 'POST',
        url: `${window.origin}/ajax-php-tutorial/backend/api/v1/user/login.php`,
        data: JSON.stringify({
            email: 'a@a.c',
            password: 'password'
        })
    }).done(function (res) {
        let val = JSON.parse(res);
        localStorage.setItem("accessToken", val.accessToken);
    });

    setTimeout(function(){
        $('#modal-login').modal('hide');
        document.getElementById('login-email').value = '';
        document.getElementById('login-password').value = '';
        document.getElementById('login-submit').style.display = "block";
        document.getElementById('login-spinner').style.display = "none";

    }, 1000);
}

function checkPassword() {
    let password = document.getElementById('register-password').value;
    let confirm = document.getElementById('register-c-password').value;
    if (password != confirm) {
        console.log(document.getElementById('alert-password').style.display = 'block');
        document.getElementById('register-submit').style.display = 'none';
    } else {
        console.log(document.getElementById('alert-password').style.display = 'none');
        document.getElementById('register-submit').style.display = 'block';
    }
}

function register() {
    document.getElementById('register-submit').style.display = "none";
    document.getElementById('register-spinner').style.display = "block";

    setTimeout(function(){
        let name = document.getElementById('register-name').value;
        let lastname = document.getElementById('register-lastname').value;
        let email = document.getElementById('register-email').value;
        let password = document.getElementById('register-password').value;
        let confirm = document.getElementById('register-c-password').value;

        document.getElementById('register-submit').style.display = "block";
        document.getElementById('register-spinner').style.display = "none";
        $('#modal-register').modal('hide');
        document.getElementById('register-name').value = '';
        document.getElementById('register-lastname').value = '';
        document.getElementById('register-email').value = '';
        document.getElementById('register-password').value = '';
        document.getElementById('register-c-password').value = '';
        document.getElementById('login-email').value = email;
        $('#modal-login').modal('show');
    }, 3000);
}
