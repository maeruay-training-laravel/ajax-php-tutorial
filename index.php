<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    AJAX-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--    END AJAX-->
<!--    Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<!--  END  Bootstrap-->

    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">



<!--    SERVER-->
    <link rel="stylesheet" href="css/index.css">
<!--END SERVER-->
    <title>Book Store</title>
</head>

<body>
<!-- modal login -->
    <div class="modal fade" id="modal-login" tabindex="100" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form onsubmit="event.preventDefault();login();">
                        <div class="form-group">
                            <label for="login-email">Email address</label>
                            <input type="email" class="form-control"  autocomplete="username"  id="login-email" aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="login-password">Password</label>
                            <input type="password" class="form-control" autocomplete="current-password" id="login-password" placeholder="Password">
                        </div>
                        <div align="right">
                            <button type="submit" id="login-submit"  class="btn btn-success">เข้าสู่ระบบ</button>
                        </div>
                        <div align="center">
                            <div class="spinner-border" id="login-spinner" role="status" style="display: none">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- end modal login -->
<!-- modal register -->
    <div class="modal fade" id="modal-register" tabindex="100" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Register</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form onsubmit="register()">
                        <div class="form-group">
                            <label>Your Name</label>
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control" id="register-name" class="form-control" placeholder="First name">
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" id="register-lastname" class="form-control" placeholder="Last name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="register-email">Email address</label>
                            <input type="email" class="form-control"  autocomplete="username"  id="register-email" aria-describedby="emailHelp" placeholder="Enter email" required>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="register-password">Password</label>
                            <input type="password" class="form-control" autocomplete="current-password" id="register-password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label for="register-c-password">Confirm Password</label>
                            <input type="password" class="form-control" onkeyup="checkPassword()" id="register-c-password" placeholder="Confirm Password" required>
                        </div>
                        <div class="alert alert-danger" role="alert" id="alert-password" style="display: none">
                            Password is not a same confirm password
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="register-check">
                            <a href="https://www.google.com" class="form-check-label" for="register-check">Term of Service</a>
                        </div>
                        <div align="right">
                            <button type="submit" id="register-submit"  class="btn btn-success" >สมัครสมาชิก</button>
                        </div>
                        <div align="center">
                            <div class="spinner-border" id="register-spinner" role="status" style="display: none">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- end modal register -->
<!-- modal add book -->
    <div class="modal fade" id="modal-add-book" tabindex="100" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Book</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- end add book -->
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">
                    <img src="/ajax-php-tutorial/img/bootstrap-solid.svg" width="30" height="30" class="d-inline-block d--xl-none align-top" alt="">
                    &nbsp;
                    Book Store
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav" id="user-item">
                        <br>
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="modal" data-target="#modal-login">Login <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#modal-register">Register</a>
                        </li>
                    </ul>
                </div>

            </nav>
        </div>
        <div class="row" id="welcome">
                <div class="welcome-page">
                    <div class="content-1 container">
                        <div class="row justify-content-center">
                            <div class="col-10 text-center">
                                <div class="welcome">
                                    <h1>Welcome</h1>
                                    <h2>Book Store</h2>
                                    <br>
                                    <div class="btn-group" role="group" aria-label="Basic example" style="z-index: 0">
                                        <a href="#dashboard" class="btn btn-primary">Dashboard</a>
                                    </div>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row" id="dashboard">
            <div class="dashboard container">
                <div class="row">
                    <div class="col-12">
                        <div class="dashboard-content">
                            <div class="card">
                                <h5 class="card-header">Book Dashboard</h5>
                                <div class="card-body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-4" style="padding-left: 0px">
                                                <h5 class="card-title" style="margin-top: 2%">Lists</h5>
                                            </div>
                                            <div class="col-5 offset-3" align="right">
                                                <button class="btn btn-primary" id="add-book" data-toggle="modal" data-target="#modal-add-book">[+] Add</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="table-responsive-xl">
                                                    <table class="table table-hover table-sm">
                                                        <thead align="center">
                                                            <tr>
                                                                <th scope="col">#</th>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Author</th>
                                                                <th scope="col">Price</th>
                                                                <th scope="col">Edit</th>
                                                                <th scope="col">Delete</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody align="center">
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">3</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">4</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">5</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">6</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">7</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">8</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">9</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">10</th>
                                                                <td>Mark</td>
                                                                <td>@mdo</td>
                                                                <td>@mdo</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-warning" style="color: white">แก้ไข</button>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-outline-danger">ลบ</button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            <nav aria-label="Page navigation example" >
                                                <ul class="pagination text-center">
                                                    <li class="page-item">
                                                        <a class="page-link" href="#" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                        </a>
                                                    </li>
                                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#" aria-label="Next">
                                                            <span aria-hidden="true">&raquo;</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <nav class="nav nav-dark bg-dark justify-content-center">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <a class="nav-link text-center" href="#">Footer</a>
                                    </div>
                                    <div class="col">
                                        <a class="nav-link text-center" href="#">Footer</a>
                                    </div>
                                    <div class="col">
                                        <a class="nav-link text-center" href="#">Footer</a>
                                    </div>
                                </div>
                            </div>

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/index.js" defer></script>
</html>