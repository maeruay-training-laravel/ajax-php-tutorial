<?php

include '../helper/server.php';
include '../helper/db.php';
allow_method('POST');

if ($request->password != $request->confirmation_password) {
    response([
        'success' => false,
        'message' => 'password is not same confirmation password'
    ]);
}

$sql = "INSERT INTO `users`
    (`name`, `lastname`, `email`, `password`)
    VALUES (
    '". $request->name ."', 
    '". $request->lastname ."', 
    '". $request->email ."', 
    '". password_hash($request->password, PASSWORD_DEFAULT) ."'
    )";

if ($conn->query($sql) === true) {
    response([
        'success' => true,
    ]);
}

response([
    'success' => false,
    'message' => $conn->error,
]);

