<?php


$path_dir = $_SERVER['DOCUMENT_ROOT'] . '/ajax-php-tutorial';

require  $path_dir . '/vendor/autoload.php';

include 'config.php';

$request = json_decode(file_get_contents("php://input"));

session_start();

function allow_method($http_method)
{
    $method = $_SERVER['REQUEST_METHOD'];

    if ($method != $http_method){
        echo json_encode([
            'status' => 405,
            'massage' => 'http method is not allow',
        ]);
        exit();
    }
}

// https://stackoverflow.com/questions/40582161/how-to-properly-use-bearer-tokens

//function getAuthorizationHeader(){
//    $headers = null;
//    if (isset($_SERVER['Authorization'])) {
//        $headers = trim($_SERVER["Authorization"]);
//    }
//    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
//        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
//    }
//    elseif (function_exists('apache_request_headers')) {
//
//    }
//    return $headers;
//}

function allow_jwt($key)
{
    $requestHeaders = apache_request_headers();
    if (empty($requestHeaders['Authorization'])) {
        response([
            'status' => 401,
            'success' => false,
            'message' => 'Unauthority',
        ]);
    }
    $jwt = explode("Bearer ", $requestHeaders['Authorization'])[1];
    $decoded = \Firebase\JWT\JWT::decode($jwt, $key, array('HS256'));
    return $decoded;
}

function response($data)
{
    echo json_encode($data);
    exit();
}

